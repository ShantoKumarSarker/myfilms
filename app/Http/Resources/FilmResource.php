<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FilmResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'release_date' => $this->release_date,
            'rating' => $this->rating,
            'ticket_price' => $this->ticket_price,
            'country'=> $this->country,
            'genre' => $this->genre,
            'photo' => $this->photo,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
            'comments' => CommentResource::collection($this->whenLoaded('comments'))

        ];
    }
}
