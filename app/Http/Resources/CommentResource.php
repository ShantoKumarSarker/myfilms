<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'user' => $this->getUserName($this->user_id),
            'body' => $this->body
        ];
    }

    private function getUserName($id) {
        $user = \App\User::where('id', '=', $id)->first();
        return $user->name;
    }
}
