<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Comment::insert([
            'film_id' => 1,//$this->getRandomFilmId(),
            'user_id' => $this->getRandomUserId(),
            'body' => 'Kingsman: The Secret Service marks the third film in a row that director Matthew Vaughn has adapted from a comic book background. His two predeceasing being Kick-Ass (2010) and X-Men: First Class (2011) - which, by all means, were pretty damn good!'
        ]);
        \App\Comment::insert([
            'film_id' => 2,//$this->getRandomFilmId(),
            'user_id' => $this->getRandomUserId(),
            'body' => 'I love a romantic comedy as much as the next girl. I go in fully expecting the cheesiness and I\'m okay with that. But "Love, Rosie" was brilliant!'
        ]);
        \App\Comment::insert([
            'film_id' => 3,//$this->getRandomFilmId(),
            'user_id' => $this->getRandomUserId(),
            'body' => 'Wild has a really intriguing premise, the idea of a solo hike does it for me but I was wary that the movie lasted over 100min. What could possibly be entertaining about a women hiking alone from the Mexican border to the Canadian Border? As it turns out quite a lot.'
        ]);
    }

    private function getRandomUserId() {
        $user = \App\User::inRandomOrder()->first();
        return $user->id;
    }

    private function getRandomFilmId() {
        $film = \App\Film::inRandomOrder()->first();
        return $film->id;
    }
}
