<?php

use Illuminate\Database\Seeder;

class FilmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Film::insert([
            'name' => 'Kingsman The Secret Service',
            'slug' => str_slug('Kingsman The Secret Service'),
            'description' => 'A spy organization recruits an unrefined, but promising street kid into the agency\'s ultra-competitive training program, just as a global threat emerges from a twisted tech genius.',
            'release_date' => '2015-02-13',
            'rating' => 5,
            'ticket_price' => 500,
            'country'=> 'USA',
            'genre' => 'Action, Adventure, Comedy',
            'photo' => 'public/uploads/images/kingsman.jpg'
        ]);
        \App\Film::insert([
            'name' => 'Love Rosie',
            'slug' => str_slug('Love Rosie'),
            'description' => 'Rosie and Alex have been best friends since they were 5, so they couldn\'t possibly be right for one another...or could they? When it comes to love, life and making the right choices, these two are their own worst enemies. ',
            'release_date' => '2014-10-22',
            'rating' => 4,
            'ticket_price' => 550,
            'country'=> 'UK',
            'genre' => 'Comedy, Romance',
            'photo' => 'public/uploads/images/love_rosie.jpg'
        ]);
        \App\Film::insert([
            'name' => 'Wild',
            'slug' => str_slug('Wild'),
            'description' => 'A chronicle of one woman\'s one thousand one hundred mile solo hike undertaken as a way to recover from a recent personal tragedy.',
            'release_date' => '2014-12-19',
            'rating' => 3,
            'ticket_price' => 450,
            'country'=> 'USA',
            'genre' => 'Adventure, Biography, Drama',
            'photo' => 'public/uploads/images/wild.jpg'
        ]);
    }
}
