@extends('layouts.master')

@section('content')
<div class="container" id="filmApp">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <img src="" class="card-img-top" alt="Photo">
                <div class="card-body">
                    <h5 class="card-title">Film Title</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href="#" class="btn btn-primary">Read More</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.22/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
    var filmApp = new Vue({
        el: '#filmApp',
        data: {
            films: '',
            next: ''
        },
        created: function () {
            axios.get('/api/films')
            .then(function (response) {
                this.films = response.data.data;
                this.next = response.data.links.next;
                console.log(this.films);
                console.log(this.next);
            })
            .catch(function (error) {
                console.log(error);
            })
        }
    });
</script>
@endsection
